const app = require('./app');
const dotenv=require('dotenv');
const connectDatabase=require("./config/database");

// Handling uncaught Exception lokie consoel.log(var) where var not defined
process.on("uncaughtException",err => {
    console.log(`Error : ${err.message}`);
    console.log(`Shutting Down our Server due to uncaughtException`);
    process.exit(1)
    
})

dotenv.config({ path: "backend/config/config.env" });

// connect Database

connectDatabase();


const server = app.listen(process.env.PORT,()=>{
    console.log(`Server is listnening on ${process.env.PORT}`)
})

// Unhandled Promise Rejection  

process.on("unhandledRejection",err => {
    console.log(`Error : ${err.message}`);
    console.log(`Shutting Down our Server due to unhandled Promise rejection`);
    server.close(()=>{
        process.exit(1)
    })
})